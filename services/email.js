const nodemailer = require('nodemailer');

const sendEmail = async (password, email) => {
  const testAccount = await nodemailer.createTestAccount();
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  });
  const mailOptions = {
    from: testAccount.user,
    to: email,
    subject: 'New password ✔',
    text: `Your new password is ${password}`,
    html: '<p>`Your new password is ${password}`</p>',
  }; ;
  transporter.sendMail(mailOptions, function(err, info) {
    if (err) {
      console.log(err);
    } else {
      console.log(info);
    }
  });
};

module.exports = {
  sendEmail,
};
