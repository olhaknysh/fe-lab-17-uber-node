const Joi = require('joi');
const {ValidationError,
  MissingFieldsError, RoleError} = require('../helpers/errors');

const typeTruckSchema = Joi.object({
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT',
      'LARGE STRAIGHT').required(),
});

const validate = (schema, req, next) => {
  if (Object.keys(req.body).length === 0) {
    return next(new MissingFieldsError('missing fields'));
  }
  const {error} = schema.validate(req.body);
  if (error) {
    const [{message}] = error.details;
    return next(new ValidationError(message));
  }
  next();
};

const validateTruckType = (req, res, next) => {
  return validate(typeTruckSchema, req, next);
};

const validateRole = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'DRIVER') {
    return next(new RoleError('You have to be a DRIVER to add new trucks'));
  }
  next();
};

module.exports= {
  validateTruckType,
  validateRole,
};
