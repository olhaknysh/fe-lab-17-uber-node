const mongoose = require('mongoose');
const {Schema} = mongoose;


const userSchema = new Schema({
  password: {
    type: String,
    required: [true, 'Password is required'],
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
    unique: true,
  },
  role: {
    type: String,
    required: [true, 'Your role has to be SHIPPER or DRIVER'],
    enum: {
      values: ['SHIPPER', 'DRIVER'],
      message: '{VALUE} is not supported',
    },
  },
  token: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  avatarURL: String,
});


const User = mongoose.model('users', userSchema);

module.exports = {User};
