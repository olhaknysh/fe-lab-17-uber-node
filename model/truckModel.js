const mongoose = require('mongoose');
const {Schema} = mongoose;


const truckSchema = new Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: {
      values: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
      message: '{VALUE} is not supported',
    },
  },
  status: {
    type: String,
    enum: {
      values: ['OL', 'IS'],
      message: '{VALUE} is not supported',
    },
  },
  createdDate: {
    type: Date,
    default: new Date().toISOString(),
  },
});


const Truck = mongoose.model('trucks', truckSchema);

module.exports = {Truck};
