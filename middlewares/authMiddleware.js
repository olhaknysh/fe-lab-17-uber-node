const jwt = require('jsonwebtoken');
require('dotenv').config();

const {NotAuthorized} = require('../helpers/errors');
const {User} = require('../model/userModal');

const authMiddleware = async (req, res, next) => {
  if (!req.headers['authorization']) {
    next(new NotAuthorized('Please provide a token'));
  }
  const [, token] = req.headers['authorization'].split(' ');

  try {
    const user = jwt.decode(token, process.env.JWT_SALT);
    if (!user) {
      next(new NotAuthorized('Invalid token'));
    }
    const userInBase = await User.findById(user._id);
    if (!userInBase.token) {
      next(new NotAuthorized('Not authorized'));
    }

    req.token = token;
    req.user = userInBase;
    next();
  } catch (err) {
    next(new NotAuthorized('Invalid token'));
  }
};

module.exports = {
  authMiddleware,
};
