const types =[
  {
    name: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  {
    name: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    name: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];


const checkDimensios = (dimensions, payload) => {
  const {width, length, height} = dimensions;

  return types.reduce((acc, type) => {
    if ( width < type.width && length < type.length && height < type.height &&
        payload < type.payload) {
      acc.push(type.name);
    }
    return acc;
  }, []);
};

module.exports = {
  checkDimensios,
};
