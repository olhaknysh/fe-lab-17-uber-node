# rUbe

**_Easy way to find loads and drivers_**

## Overview

This API will help you with:

- manage your loads
- manage your trucks

## Getting started

Please use (https://fierce-beach-10656.herokuapp.com/api) to get access

### Authorize first

URL | Action | Returns
--- | --- | ---
**post** `/api/auth/register` | Create new profile | "Profile created successfully"
**post** `/api/auth/login` | Login into the system | jwt_token of the user
**post** `/api/auth/forgot_password` | Forgot password option | Send new password on the email

### Information about user

URL | Action | Returns
--- | --- | ---
**get** `/api/users/me` | Requst own profile info | User info
**delete** `/api/users/login` | Delete own profile| "Profile deleted successfully"
**patch** `/api/users/me/password` | Change user's password | "Password changed successfully"

### Information about trucks(available only for drivers)

URL | Action | Returns
--- | --- | ---
**get** `/api/trucks` | Retrieve the list of trucks | All created trucks
**post** `/api/trucks` | Add Truck for User| "Truck created successfully"
**get** `/api/trucks/{id}` | Get user's truck by id | Specific truck by id
**put** `/api/trucks/{id}` | Update user's truck by id | "Truck details changed successfully"
**delete** `/api/trucks/{id}` | Delete user's truck by id | "Truck deleted successfully"
**post** `/api/trucks/{id}/assign` | Assign truck to user by id | "Truck assigned successfully"

### Information about loads

URL | Action | Returns
--- | --- | ---
**get** `/api/loads` | Retrieve the list of loads | List of loads
**get** `/api/loads/{id}` | Get user's Load by id | Specific load by id

#### Actions available for shipper

URL | Action | Returns
--- | --- | ---
**post** `/api/loads` | Add Load for Use | "Load created successfully"
**put** `/api/loads/{id}` | Update user's load by id | "Load details changed successfully"
**delete** `/api/loads/{id}` | Delete user's load by id | "Load deleted successfully"
**post** `/api/loads/{id}/post` | Post a user's load by id, search for drivers | Info about result
**get** `/api/loads/{id}/shipping_info` | Get user's Load shipping info by id | Detailed info about shipment

#### Actions available for driver

URL | Action | Returns
--- | --- | ---
**get** `/api/loads/active` | Retrieve the active load | Info about load
**patch** `/api/loads/active/state` | Iterate to next Load state | "Load state changed to 'En route to Delivery'"






