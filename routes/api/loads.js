const express = require('express');
const router = express.Router();

const {
  getLoadsController,
  addLoadController,
  getActiveLoadsController,
  iterateToNextStateLoadController,
  getLoadByIdController,
  updateLoadByIdController,
  deleteLoadByIdController,
  postLoadByIdController,
  getLoadInfoByIdController,
} = require('../../controllers/loads');

const {asyncWrapper} = require('../../helpers/apiHelpers');
const {authMiddleware} = require('../../middlewares/authMiddleware');
const {validateLoad, validateRoleDriver,
  validateRoleShipper} = require('../../validation/loadsValidation');

router.get('/', authMiddleware,
    asyncWrapper(getLoadsController));
router.post('/', authMiddleware, validateRoleShipper, validateLoad,
    asyncWrapper(addLoadController));
router.get('/active', authMiddleware, validateRoleDriver,
    asyncWrapper(getActiveLoadsController));
router.patch('/active/state', authMiddleware, validateRoleDriver,
    asyncWrapper(iterateToNextStateLoadController));
router.get('/:id', authMiddleware,
    asyncWrapper(getLoadByIdController));
router.put('/:id', authMiddleware, validateRoleShipper, validateLoad,
    asyncWrapper(updateLoadByIdController));
router.delete('/:id', authMiddleware, validateRoleShipper,
    asyncWrapper(deleteLoadByIdController));
router.post('/:id/post', authMiddleware, validateRoleShipper,
    asyncWrapper(postLoadByIdController));
router.get('/:id/shipping_info', authMiddleware, validateRoleShipper,
    asyncWrapper(getLoadInfoByIdController));


module.exports = router;
