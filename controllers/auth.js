const {registerUser, loginUser,
  forgotPasswordUser} =require('../services/authService');

const registerUserController = async (req, res) =>{
  const {email, password, role} = req.body;
  await registerUser(email, password, role);
  res.status(200).json({'message': 'Profile created successfully'});
};

const loginUserController = async (req, res) =>{
  const {email, password} = req.body;
  const token = await loginUser(email, password);
  res.status(200).json({jwt_token: token});
};

const forgotPasswordControlled = async (req, res) =>{
  const {email} = req.body;
  await forgotPasswordUser(email);
  res.status(200).json({'message': 'New password sent to your email address'});
};

module.exports ={
  registerUserController,
  loginUserController,
  forgotPasswordControlled,
};
