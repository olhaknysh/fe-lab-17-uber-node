const Joi = require('joi');
const {ValidationError,
  MissingFieldsError} = require('../helpers/errors');

const registerUserSchema = Joi.object({
  email: Joi.string()
      .email({
        minDomainSegments: 2,
        tlds: {allow: ['com', 'net']},
      })
      .required(),

  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
});

const loginUserSchema = Joi.object({
  email: Joi.string()
      .email({
        minDomainSegments: 2,
        tlds: {allow: ['com', 'net']},
      })
      .required(),

  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
});

const passwordForgorUserSchema = Joi.object({
  email: Joi.string()
      .email({
        minDomainSegments: 2,
        tlds: {allow: ['com', 'net']},
      })
      .required(),
});

const passwordChangeUserSchema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required()});


const validate = (schema, body, next) => {
  if (Object.keys(body).length === 0) {
    return next(new MissingFieldsError('missing fields'));
  }
  const {error} = schema.validate(body);
  if (error) {
    const [{message}] = error.details;
    return next(new ValidationError(message));
  }
  next();
};

const validateRegisterUser = (req, res, next) => {
  return validate(registerUserSchema, req.body, next);
};

const validateLoginUser = (req, res, next) => {
  return validate(loginUserSchema, req.body, next);
};

const validatePasswordForgotUser = (req, res, next) => {
  return validate(passwordForgorUserSchema, req.body, next);
};

const validatePasswordChangeUser = (req, res, next) => {
  return validate(passwordChangeUserSchema, req.body, next);
};


module.exports = {
  validateRegisterUser,
  validateLoginUser,
  validatePasswordForgotUser,
  validatePasswordChangeUser,
};
