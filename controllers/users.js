const {updateUser, removeUser} =require('../services/usersService');
const {PasswordError} = require('../helpers/errors');

const getUserController = async (req, res) => {
  const {email, role, _id, createdDate} = req.user;

  const user = {
    _id: _id.toString(),
    role,
    email,
    createdDate,
  };
  res.status(200).json({user});
};

const changeUserController = async (req, res) => {
  const {_id} = req.user;
  const {oldPassword, newPassword} = req.body;

  if (oldPassword === newPassword) {
    throw new PasswordError('New password is the same as the old one');
  }

  await updateUser(_id, oldPassword, newPassword);

  res.status(200).json({message: 'Password changed successfully'});
};

const deleteUserController = async (req, res) => {
  const {_id} = req.user;

  await removeUser(_id);

  res.status(200).json({message: 'Profile deleted successfully'});
};

module.exports ={
  getUserController,
  changeUserController,
  deleteUserController,
};
