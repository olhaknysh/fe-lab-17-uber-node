const express = require('express');
const router = express.Router();

const {
  getTrucksController,
  addTruckController,
  getTruckByIdController,
  updateTruckByIdController,
  deleteTruckByIdController,
  assignTruckByIdController,
} = require('../../controllers/trucks');

const {asyncWrapper} = require('../../helpers/apiHelpers');
const {authMiddleware} = require('../../middlewares/authMiddleware');
const {validateTruckType,
  validateRole} =require('../../validation/trucksValidation');

router.get('/', authMiddleware, validateRole,
    asyncWrapper(getTrucksController));
router.post('/', authMiddleware, validateRole, validateTruckType,
    asyncWrapper(addTruckController));
router.get('/:id', authMiddleware, validateRole,
    asyncWrapper(getTruckByIdController));
router.put('/:id', authMiddleware, validateRole,
    validateTruckType,
    asyncWrapper(updateTruckByIdController));
router.delete('/:id', authMiddleware, validateRole,
    asyncWrapper(deleteTruckByIdController));
router.post('/:id/assign', authMiddleware, validateRole,
    asyncWrapper(assignTruckByIdController));


module.exports = router;
