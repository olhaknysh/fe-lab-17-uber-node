const Joi = require('joi');
const {ValidationError,
  MissingFieldsError, RoleError} = require('../helpers/errors');

const loadSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object().keys({
    width: Joi.number(),
    length: Joi.number(),
    height: Joi.number(),
  }).required(),
});

const validate = (schema, req, next) => {
  if (Object.keys(req.body).length === 0) {
    return next(new MissingFieldsError('missing fields'));
  }
  const {error} = schema.validate(req.body);
  if (error) {
    const [{message}] = error.details;
    return next(new ValidationError(message));
  }
  next();
};

const validateLoad = (req, res, next) => {
  return validate(loadSchema, req, next);
};

const validateRoleDriver = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'DRIVER') {
    return next(new RoleError('You have to be a DRIVER to have access'));
  }
  next();
};

const validateRoleShipper = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'SHIPPER') {
    return next(new RoleError('You have to be a SHIPPER to have access'));
  }
  next();
};

module.exports= {
  validateLoad,
  validateRoleDriver,
  validateRoleShipper,
};
