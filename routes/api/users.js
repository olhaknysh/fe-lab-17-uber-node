const express = require('express');
const router = express.Router();

const {
  getUserController,
  deleteUserController,
  changeUserController,
} = require('../../controllers/users');

const {asyncWrapper} = require('../../helpers/apiHelpers');
const {authMiddleware} = require('../../middlewares/authMiddleware');
const {validatePasswordChangeUser} =require('../../validation/userValidation');

router.get('/me', authMiddleware, asyncWrapper(getUserController));
router.delete('/me', authMiddleware, asyncWrapper(deleteUserController));
router.patch('/me/password',
    authMiddleware, validatePasswordChangeUser,
    asyncWrapper(changeUserController));

module.exports = router;
