const mongoose = require('mongoose');
const {Schema} = mongoose;


const loadSchema = new Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    enum: {
      values: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
      message: '{VALUE} is not supported',
    },
    default: 'NEW',
  },
  state: {
    type: String,
    enum: {
      values: ['En route to Pick Up', 'Arrived to Pick Up',
        'En route to delivery', 'Arrived to delivery'],
      message: '{VALUE} is not supported',
    },
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    type: mongoose.SchemaTypes.Mixed,
  },
  logs: {
    type: Array,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});


const Load = mongoose.model('loads', loadSchema);

module.exports = {Load};
