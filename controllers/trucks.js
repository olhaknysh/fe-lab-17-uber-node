const {getAllTrucks, addTruck, getTruckById,
  updateTruckById, deleteTruckById,
  assignTruckById} = require('../services/trucks');

const getTrucksController = async (req, res) =>{
  const {_id} = req.user;

  const trucks = await getAllTrucks(_id);

  res.status(200).json({trucks});
};

const addTruckController = async (req, res) =>{
  const {_id} = req.user;
  const {type} = req.body;

  await addTruck(type, _id);

  res.status(200).json({message: 'Truck created successfully'});
};

const getTruckByIdController = async (req, res) =>{
  const {id} = req.params;
  const truck = await getTruckById(id);
  res.status(200).json({truck: truck[0]});
};

const updateTruckByIdController = async (req, res) =>{
  const {id} = req.params;
  const {type} = req.body;

  await updateTruckById(id, type);

  res.status(200).json({message: 'Truck details changed successfully'});
};

const deleteTruckByIdController = async (req, res) =>{
  const {id} = req.params;
  await deleteTruckById(id);
  res.status(200).json({message: 'Truck deleted successfully'});
};

const assignTruckByIdController = async (req, res) =>{
  const {_id} = req.user;
  const {id} = req.params;

  await assignTruckById(_id, id);

  res.status(200).json({message: 'Truck assigned successfully'});
};

module.exports = {
  getTrucksController,
  addTruckController,
  getTruckByIdController,
  updateTruckByIdController,
  deleteTruckByIdController,
  assignTruckByIdController,
};
