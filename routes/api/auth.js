const express = require('express');
const router = express.Router();

const {validateRegisterUser,
  validateLoginUser,
  validatePasswordForgotUser} = require('../../validation/userValidation');

const {
  registerUserController,
  loginUserController,
  forgotPasswordControlled,
} = require('../../controllers/auth');

const {asyncWrapper} = require('../../helpers/apiHelpers');

router.post('/register', validateRegisterUser,
    asyncWrapper(registerUserController));
router.post('/login', validateLoginUser, asyncWrapper(loginUserController));
router.post('/forgot_password', validatePasswordForgotUser,
    asyncWrapper(forgotPasswordControlled));


module.exports = router;
