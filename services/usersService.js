const bcrypt = require('bcryptjs');

const { User } = require('../model/userModal.js');
const { PasswordError, DeleteError } = require('../helpers/errors');

const updateUser = async (id, oldPassword, newPassword) => {
  const user = await User.findById(id);
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new PasswordError('Old password is wrong');
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
};

const removeUser = async (id) => {
  await User.findByIdAndDelete(id, function (err) {
    if (err) {
      throw new DeleteError('Problem with deleting user');
    }
  });
};

module.exports = {
  updateUser,
  removeUser,
};
