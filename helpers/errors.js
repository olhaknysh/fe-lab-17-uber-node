class CustomError extends Error {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class NotAuthorized extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class UserWithPasswordAlreadyExists extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class ValidationError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class NotFound extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class PasswordError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class DeleteError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class RoleError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class MissingFieldsError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}


class DoubleAssignError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class NoLoad extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class UnableToUpdate extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}


module.exports = {
  CustomError,
  NotAuthorized,
  UserWithPasswordAlreadyExists,
  ValidationError,
  NotFound,
  PasswordError,
  DeleteError,
  RoleError,
  MissingFieldsError,
  DoubleAssignError,
  NoLoad,
  UnableToUpdate,
};
