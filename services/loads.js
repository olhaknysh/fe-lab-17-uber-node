const ObjectId = require('mongodb').ObjectId;
const { Load } = require('../model/loadModel');
const { Truck } = require('../model/truckModel');
const { NotFound, NoLoad } = require('../helpers/errors');
const { checkDimensios } = require('../helpers/truckTypes');

const getLoads = async (role, userId, offset = 0, limit = 10) => {
  let innerOffset = Number(offset);
  if (innerOffset > 50) {
    innerOffset = 50;
  }
  let loads;
  if (role === 'SHIPPER') {
    loads = await Load.find({ created_by: userId })
      .skip(innerOffset)
      .limit(Number(limit));
  } else if (role === 'DRIVER') {
    loads = await Load.find({ assigned_to: userId })
      .skip(innerOffset)
      .limit(Number(limit));
  }
  return loads;
};

const addLoad = async (
  userId,
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions
) => {
  const load = new Load({
    created_by: userId,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    logs: [{ message: 'You have created the load' }],
  });
  await load.save();
};

const postLoad = async (id) => {
  try {
    const load = await Load.findById(id);
    if (load.status !== 'NEW') {
      throw new Error('You have already posted the load');
    }
    load.status = 'POSTED';
    const poastMessage = {
      message: 'You have posted the load',
      time: Date.now(),
    };
    load.logs.push(poastMessage);
    await load.save();
    if (load.length === 0) {
      throw new Error(`Load with id ${id} is not found`);
    }

    const truckTypes = checkDimensios(load.dimensions, load.payload);
    if (truckTypes.length === 0) {
      const loadMessage = {
        message: 'No trucks are suited for your sizes',
        time: Date.now(),
      };

      load.logs.push(loadMessage);
      load.status = 'NEW';
      await load.save();

      throw new Error('Your load is too big for our trucks');
    }
    const driver = await Truck.findOne({
      type: { $in: truckTypes },
      status: 'IS',
      assigned_to: { $nin: [null] },
    });
    if (driver) {
      driver.status = 'OL';
      load.status = 'ASSIGNED';
      load.state = 'En route to Pick Up';
      load.assigned_to = driver.assigned_to;
      const loadMessage = {
        message: `Load assigned to driver with id ${driver.assigned_to}`,
        time: Date.now(),
      };
      load.logs.push(loadMessage);
      await driver.save();
      await load.save();
    } else {
      const loadMessage = {
        message: 'Could not find driver',
        time: Date.now(),
      };

      load.logs.push(loadMessage);
      load.status = 'NEW';
      load.assined_to = '';
      await load.save();
      throw new Error('No drivers were found');
    }
  } catch (err) {
    throw new NotFound(err.message);
  }
};

const getActiveLoads = async (userId) => {
  const load = await Load.findOne({ assigned_to: userId, status: 'ASSIGNED' });
  if (!load) {
    return {};
  }
  return load;
};

const iterateToNextStateLoad = async (userId) => {
  const load = await Load.findOne({ assigned_to: userId, status: 'ASSIGNED' });
  if (!load) {
    return;
  }
  load.state = 'Arrived to delivery';
  await load.save();

  load.status = 'SHIPPED';
  await load.save();

  const truck = await Truck.findOne({ assigned_to: userId });
  truck.status = 'IS';
  await truck.save();
};

const getLoadInfoById = async (loadId) => {
  const load = await Load.findOne({
    _id: new ObjectId(loadId),
    status: 'ASSIGNED',
  });
  if (!load) {
    throw new NotFound(`No active load with id ${loadId} found`);
  }
  const truck = await Truck.findOne({ assigned_to: load.assigned_to });
  return { load, truck };
};

const getLoadById = async (id) => {
  const load = await Load.findOne({ _id: new ObjectId(id) });
  if (!load) {
    throw new NotFound(`Load with id ${id} is not found`);
  }
  return load;
};

const updateLoadById = async (
  loadId,
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions
) => {
  const load = await Load.findOne({ _id: new ObjectId(loadId), status: 'NEW' });
  if (!load) {
    throw new NotFound(`New load with id ${loadId} is not found`);
  }
  load.name = name;
  load.payload = payload;
  load.pickup_address = pickup_address;
  load.delivery_address = delivery_address;
  load.dimensions = dimensions;
  await load.save();
};

const deleteLoadById = async (id) => {
  const load = await Load.findOne({ _id: new ObjectId(id), status: 'NEW' });
  if (!load) {
    throw new NotFound(`New load with id ${id} is not found`);
  }
  await Load.findByIdAndDelete(id);
};

module.exports = {
  getLoads,
  addLoad,
  postLoad,
  getActiveLoads,
  getLoadInfoById,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  iterateToNextStateLoad,
};
