const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const gravatar = require('gravatar');
const fs = require('fs');
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const Jimp = require('jimp');
const { sendEmail } = require('./email');

require('dotenv').config();

const { User } = require('../model/userModal.js');
const {
  NotAuthorized,
  UserWithPasswordAlreadyExists,
  NotFound,
} = require('../helpers/errors');

const registerUser = async (email, password, role) => {
  const userWithSuchPassword = await User.findOne({ email });
  if (userWithSuchPassword) {
    throw new UserWithPasswordAlreadyExists('Email in use');
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
    avatarURL: gravatar.url(this.email, { s: '250' }, true),
  });
  await user.save();

  return user;
};

const loginUser = async (email, password) => {
  const user = await User.findOne({ email });

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new NotFound(`Email or password is wrong`);
  }

  const token = jwt.sign(
    {
      _id: user._id,
    },
    process.env.JWT_SALT
  );
  await user.updateOne({ token: token });

  return token;
};

const forgotPasswordUser = async (email) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new NotAuthorized(`User with email ${email} is not found`);
  }

  const newPassword = uuidv4();
  const hashedPassword = await bcrypt.hash('newPassword', 10);

  await user.updateOne({ password: hashedPassword });

  sendEmail(newPassword, email);
};

const updateAvatar = async (user, file, url) => {
  const { path: pathAvatar } = file;
  const newAvatarPath = path.resolve('./public/avatars');

  const img = await Jimp.read(pathAvatar);
  await img.autocrop().cover(250, 250).writeAsync(pathAvatar);

  const [, extension] = file.originalname.split('.');
  const newNameAvatar = `${uuidv4()}.${extension}`;

  await fs.rename(pathAvatar, `${newAvatarPath} /${newNameAvatar}`, (err) => {
    if (err) throw err;
  });

  const newUrl = `${url}/${newNameAvatar}`;
  await user.updateOne({ avatarURL: newUrl });
};

module.exports = {
  registerUser,
  loginUser,
  updateAvatar,
  forgotPasswordUser,
};
