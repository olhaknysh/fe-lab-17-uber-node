const {getLoads, addLoad, postLoad,
  getActiveLoads, getLoadInfoById, getLoadById,
  updateLoadById, deleteLoadById,
  iterateToNextStateLoad} = require('../services/loads');

const getLoadsController = async (req, res) =>{
  const {_id, role} = req.user;
  const {offset, limit} = req.query;
  const loads = await getLoads(role, _id, offset, limit);
  res.status(200).json({loads});
};

const addLoadController = async (req, res) =>{
  const {name, payload, pickup_address,
    delivery_address, dimensions} = req.body;
  const {_id} = req.user;

  await addLoad(_id, name, payload, pickup_address,
      delivery_address, dimensions);

  res.status(200).json({message: 'Load created successfully'});
};

const getActiveLoadsController = async (req, res) =>{
  const {_id} = req.user;
  const load = await getActiveLoads(_id);
  res.status(200).json({load});
};

const iterateToNextStateLoadController = async (req, res) =>{
  const {_id} = req.user;
  await iterateToNextStateLoad(_id);
  res.status(200)
      .json({message: 'Load state changed to "En route to Delivery"'});
};

const getLoadByIdController = async (req, res) =>{
  const {id} = req.params;
  const load = await getLoadById(id);
  res.status(200).json({load});
};

const updateLoadByIdController = async (req, res) =>{
  const {id} = req.params;
  const {name, payload, pickup_address,
    delivery_address, dimensions} = req.body;
  await updateLoadById(id, name, payload, pickup_address,
      delivery_address, dimensions);
  res.status(200).json({message: 'Load details changed successfully'});
};

const deleteLoadByIdController = async (req, res) =>{
  const {id} = req.params;
  await deleteLoadById(id);
  res.status(200).json({message: 'Load deleted successfully'});
};

const postLoadByIdController = async (req, res) =>{
  const {id} = req.params;
  await postLoad(id);

  res.status(200).json({'message': 'Load posted successfully',
    'driver_found': true});
};

const getLoadInfoByIdController = async (req, res) => {
  const {id} = req.params;

  const {load, truck} = await getLoadInfoById(id);
  res.status(200).json({load, truck});
};

module.exports ={
  getLoadsController,
  addLoadController,
  getActiveLoadsController,
  iterateToNextStateLoadController,
  getLoadByIdController,
  updateLoadByIdController,
  deleteLoadByIdController,
  postLoadByIdController,
  getLoadInfoByIdController,
};
