const ObjectId = require('mongodb').ObjectId;
const {Truck} = require('../model/truckModel');
const {NotFound, DoubleAssignError,
  UnableToUpdate} = require('../helpers/errors');

const getAllTrucks = async (userId) =>{
  const trucks = await Truck.find({created_by: userId.toString()});
  return trucks;
};

const addTruck = async (type, userId) =>{
  const truck = new Truck({
    status: 'IS',
    created_by: userId.toString(),
    type,
  });
  await truck.save();
};

const getTruckById = async (id) =>{
  try {
    const truck = await Truck.find({_id: new ObjectId(id)});
    if (truck.length === 0) {
      throw new Error();
    }
    return truck;
  } catch (err) {
    throw new NotFound(`Truck with id ${id} is not found`);
  }
};

const updateTruckById = async (id, type) =>{
  try {
    const truck = await Truck.findOne({_id: new ObjectId(id)});
    if (truck.status === 'OL') {
      throw new Error(
          'You are not allowed to update information when you are on load');
    }
    if (!truck) {
      throw new Error(`Truck with id ${id} is not found`);
    }
    await Truck.findOneAndUpdate({_id: new ObjectId(id)}, {type});
  } catch (err) {
    throw new UnableToUpdate(err.message);
  }
};

const deleteTruckById = async (id) =>{
  try {
    const truck = await Truck.findOne({_id: new ObjectId(id)});
    if (!truck) {
      throw new Error(`Truck with id ${id} is not found`);
    }
    if (truck.status === 'OL') {
      throw new Error(
          'You are not allowed to update information when you are on load');
    }

    await Truck.deleteOne({_id: new ObjectId(id)});
  } catch (err) {
    throw new UnableToUpdate(err.message);
  }
};

const assignTruckById = async (userId, truckId) => {
  const assignedTruck = await Truck.find({assigned_to: userId});
  if (assignedTruck.length === 0) {
    try {
      await Truck.findOneAndUpdate({_id: new ObjectId(truckId)},
          {assigned_to: userId, status: 'IS'});
    } catch (err) {
      throw new NotFound(`Truck with id ${id} is not found`);
    }
  } else {
    throw new DoubleAssignError('Only one truck can be assigned to the DRIVER');
  }
};

module.exports = {
  addTruck,
  getAllTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
